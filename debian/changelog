libcddb-get-perl (2.28-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libcddb-get-perl: Add Multi-Arch: foreign.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 06 Dec 2022 18:49:31 +0000

libcddb-get-perl (2.28-3) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Ryan Niebur from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * Rename debian/NEWS.Debian to debian/NEWS to get it actually installed.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * Add patch to replace freedb server with gnudb server.
    Thanks to Stephan Seitz for the bug report and Elimar Riesebieter for the
    patch. (Closes: #963046)
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Tue, 21 Jul 2020 19:11:16 +0200

libcddb-get-perl (2.28-2) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Slightly rewrite sentence in long description to fix grammatical error.
    Thanks to Davide Prina <davide.prina@gmail.com> for the report
    (Closes: #698592)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Florian Schlichting ]
  * Add no-debug.patch (Closes: #708289) and
    do-not-include-current-directory.patch (from upstream)
  * Email change: Florian Schlichting -> fsfs@debian.org
  * Bump dh compat to level 9
  * Declare compliance with Debian Policy 3.9.8
  * Mark package autopkgtest-able
  * Add spelling.patch, fixing a POD typo

 -- Florian Schlichting <fsfs@debian.org>  Mon, 02 May 2016 23:08:01 +0200

libcddb-get-perl (2.28-1) unstable; urgency=low

  [ Ryan Niebur ]
  * Update ryan52's email address

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Florian Schlichting ]
  * Imported Upstream version 2.28.
  * Switched to source format 3.0 (quilt).
  * Bumped debhelper compatibility to level 8 (no changes necessary).
  * Bumped Standards-Version to 3.9.3 (use copyright-format 1.0, delete
    versioned dependency on Perl 5.8).
  * Fixed typo in cddbget.1.
  * Added myself to Uploaders and copyright.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Sat, 10 Mar 2012 17:13:15 +0100

libcddb-get-perl (2.27-1) unstable; urgency=low

  [ Ryan Niebur ]
  * Take over for the Debian Perl Group; Closes: #533086 -- RFA
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza);
    ${misc:Depends} to Depends: field. Changed: Maintainer set to Debian
    Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
  * debian/watch: use dist-based URL.
  * New upstream release (Closes: #415415)
  * Add myself to Uploaders
  * Debian Policy 3.8.1
  * dh7
  * machine parsable copyright format
  * update debian specific manpage

  [ Salvatore Bonaccorso ]
  * Bump Standards-Version to 3.8.2 (No changes)

 -- Ryan Niebur <ryanryan52@gmail.com>  Sun, 21 Jun 2009 12:26:23 -0700

libcddb-get-perl (2.23-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS with Perl 5.10.  Closes: #467716

 -- Mark Hymers <mhy@debian.org>  Sat, 05 Apr 2008 21:08:53 +0100

libcddb-get-perl (2.23-2) unstable; urgency=low

  * New maintainer. (closes: #297428)
  * Added cddb.pl man page.

 -- Lucas Wall <lwall@debian.org>  Tue,  1 Mar 2005 03:42:26 -0300

libcddb-get-perl (2.23-1) unstable; urgency=low

  * New upstream

 -- Kevin M. Rosenberg <kmr@debian.org>  Sun, 14 Sep 2003 21:17:38 -0600

libcddb-get-perl (2.22-1) unstable; urgency=low

  * New upstream (closes: 204396)

 -- Kevin M. Rosenberg <kmr@debian.org>  Mon, 18 Aug 2003 00:24:59 -0600

libcddb-get-perl (2.20-1) unstable; urgency=low

  * New upstream

 -- Kevin M. Rosenberg <kmr@debian.org>  Sat,  9 Aug 2003 19:19:15 -0600

libcddb-get-perl (2.11-1) unstable; urgency=low

  * Initial Release.

 -- Kevin M. Rosenberg <kmr@debian.org>  Fri,  6 Dec 2002 13:33:32 -0700
